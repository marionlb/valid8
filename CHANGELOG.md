# Changelog

## 1.1

**Date**: Feburary 7th, 2019

* Renamed the package and cli to 'valid8'
* The CLI call `valid8 validate` is renamed `valid8 apply`
* `rbv` CLI command still works but the `validate` portion is also now `rbv apply` 
* Improved CLI help message

## 1.0

**Date**: Feburary 5th, 2019

Initial release.

Supports: 
* YAML-specified rules 
* `rbv` CLI 
* Usage: `rbv [-d path/to/dir rules.yml`

 
